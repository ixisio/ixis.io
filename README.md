# My Personal Sandbox and Place to talk about Things

## Why a new release of my website?

I created a full automated, linted, deploy'able and over-configured blog page with Node.js and in 2013. This time the main focus will be on **Simplicity**.
Of course I'll use some tools for optimizing loading performance..and yeah, there will be a strategy to continuous deliver my site. But apart from that it's just plain simple HTML and CSS.

## Dependencies
* A browser

## DevDependencies
* Node.js
* node-sass
* Gulp.js
