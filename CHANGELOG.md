# ixis.io - CHANGELOG

## HEAD
* Add initial version
    - Build System with Gulp
    - Content-Management with Kirby
    - CSS Pre-Processing using libsass
    - JS Module Loader with browserify
