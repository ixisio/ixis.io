
// Require Vendors
var $ = require('jquery');
var Picturefill = require('picturefill');

// Requrie Modules
// var Navigation = require('./_modules/_navigation/navigation');
var Slider = require('./_modules/_slider/slider');

// Construct
// new Navigation();

// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-72820202-1', 'auto');
ga('send', 'pageview');
